class Calculadora : ICalculadora
{
    public int num1 { get; set; }
    public int num2 { get; set; }

    public Calculadora(int num1, int num2){
        this.num1 = num1;
        this.num2 = num2;
    }

    public void Adicao()
    {
        Console.WriteLine(num1 + num2);
    }

    public void Subtracao()
    {
        Console.WriteLine(num1 - num2);
    }

    public void Multiplicacao()
    {
        Console.WriteLine(num1 * num2);
    }

    public void Divisao()
    {
        Console.WriteLine(num1 / num2);
    }
}